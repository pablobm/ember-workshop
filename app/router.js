import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('players', {path: '/'}, function() {
    this.route('new', {path: 'players/new'});
    this.route('edit', {path: 'players/:id/edit'});
  });
  this.route('results', function() {
    this.route('new');
  });
});

export default Router;
