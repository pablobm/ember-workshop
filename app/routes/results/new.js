import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.store.createRecord('result');
  },

  actions: {
    save(record) {
      record.save()
        .then(() => {
          this.transitionTo('players');
        });
    }
  }
});
