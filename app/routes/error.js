import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    reload() {
      this.transitionTo('players');
    },
  },
});
