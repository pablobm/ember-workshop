import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
    return this.store.findRecord('player', params.id);
  },

  actions: {
    save(record) {
      record.save()
        .then(() => this.transitionTo('players'))
        .catch(error => console.error("Error saving player", error));
    },

    willTransition() {
      const record = this.controller.get('model');
      record.rollbackAttributes();
    },
  },
});
