import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.store.createRecord('player');
  },

  actions: {
    save(record) {
      record.save()
        .then(() => this.transitionTo('players'))
        .catch(error => console.error("Error saving player", error));
    },

    willTransition() {
      const record = this.controller.get('model');
      if (record.get('isNew')) {
        this.store.unloadRecord(record);
      }
    },
  },
});
