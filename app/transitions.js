export default function() {
  this.transition(
    this.fromRoute('loading'),
    this.fromRoute('error'),
    this.use('scale')
  );

  this.transition(
    this.fromRoute('players.index'),
    this.toRoute('players.new'),
    this.use('toUp'),
    this.reverse('toDown')
  );
}
