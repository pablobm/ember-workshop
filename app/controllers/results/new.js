import Ember from 'ember';

export default Ember.Controller.extend({
  players: Ember.computed(function() {
    return this.store.findAll('player');
  }),

  actions: {
    winnerChange(player) {
      this.set('model.winner', player);
    },

    loserChange(player) {
      this.set('model.loser', player);
    },
  },
});
