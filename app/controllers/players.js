import Ember from 'ember';

export default Ember.Controller.extend({
  queryParams: ['sortBy', 'sortDir'],

  sortBy: 'score',
  sortDir: 'desc',

  playersSortingKeyDir: Ember.computed('sortBy', 'sortDir', function() {
    return [[this.get('sortBy'), this.get('sortDir')].join(':')];
  }),
});
