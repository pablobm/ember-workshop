import Ember from 'ember';

export default Ember.Component.extend({
  player: null,

  saveIsDisabled: Ember.computed.not('player.name'),

  actions: {
    save() {
      this.sendAction('on-save', this.get('player'));
    },
  },
});
