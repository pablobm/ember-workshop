import Ember from 'ember';

export default Ember.Component.extend({
  players: [],
  orderRule: ['score:asc'],

  orderedPlayers: Ember.computed.sort('players', 'orderRule'),
});
